package org.green;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketsTry5Application {

	public static void main(String[] args) {
		SpringApplication.run(WebsocketsTry5Application.class, args);
	}
}
