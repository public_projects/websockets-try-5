/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.green.websocket;


import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

/**
 *
 * Configures the endpoints necessary to create the WebSockets connections.
 *
 * <div>
 * <b>@Configuration</b>
 * <li>You know that this will mark the class as configuration for the Spring
 * container.</li>
 * </div>
 * <div>
 * <b>@EnableWebSocketMessageBroker</b>
 * <li>This annotation will use the autoconfiguration to create all the
 * necessary artifacts to enable broker-backed messaging over WebSockets using a
 * very high-level messaging sub-protocol. If you need to customize the
 * endpoints you need to override the methods from the
 * <code>AbstractWebSocketMessageBrokerConfigurer</code> class
 * </li>
 * </div>
 *
 * <div>
 * <b>AbstractWebSocketMessageBrokerConfigurer</b>
 * <li>The <code>WebSocketConfig</code> is extending from this class. It will
 * override methods to customize the protocols and endpoints.</li>
 * </div>
 * <div>
 *
 * @author greenhorn
 */
@Configuration // You know that this will mark the class as configuration for the Spring container.  
@EnableWebSocketMessageBroker // This annotation will use the autoconfiguration to create all the necessary artifacts to enable broker-backed messaging over WebSockets using a very high-level messaging sub-protocol. If you need to customize the endpoints you need to override the methods from the AbstractWebSocketMessageBrokerConfigurer 
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    /**
     * Will register the Stomp (https://stomp.github.io/) endpoint; in this case
     * it will register the <code>/stomp</code> endpoint and use the JavaScript
     * library SockJS (https://github.com/ sockjs).
     * 
     * For {@code setAllowedOrigins} 
     * see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html#websocket-server-allowed-origins">websocket-server-allowed-origins</a>  
     *
     * @param registry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/stomp").setAllowedOrigins("http://localhost:4200").withSockJS();
    }

    /**
     * Will configure the message broker options. In this case, it will enable
     * the broker in the <code>/topic</code> endpoint. This means that the clients who want
     * to use the WebSockets broker need to use the <code>/topic</code> to connect.
     *
     * @param config
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }
}

