/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.green.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author greenhorn
 */
@RestController // This annotation marks the class as a REST controller. This will register endpoints marked with the  @RequestMapping  annotation. 
public class WebSocketController {

    @Autowired
    Producer producer;

    /**
     * @RequestMapping("/send/{topic}") . This annotation is the REST endpoint.
     * In this case, it requires the topic path variable. The method sender
     * accepts two parameters—the topic that is marked as @PathVariable that
     * matches the endpoint signature (from the @RequestMapping annotation) and
     * the message that is annotated with @RequestParameter , meaning that this
     * value will be passed as an url param. The sender method uses the Producer
     * instance to send the message to the specified topic.
     * @param topic
     * @param message
     * @return
     */
    @RequestMapping("/send/{topic}")
    public String sender(@PathVariable String topic, @RequestParam String message) {
        producer.sendMessageTo(topic, message);
        return "OK-Sent";
    }

    @RequestMapping("/send")
    public String sender2(@RequestParam String topicName, @RequestParam String message) {
        producer.sendMessageTo(topicName, message);
        return "OK-Sent";
    }
}
