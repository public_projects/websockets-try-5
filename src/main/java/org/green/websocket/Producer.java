/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.green.websocket;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * Producer that will send the message to the HTML.
 *
 * @author greenhorn
 */
@Component // This annotation will register the  Producer  class as the bean for the Spring container.  
public class Producer {

    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/ yyyy HH:mm:ss");

    /**
     * SimpMessagingTemplate . This class is an implementation of the
     * SimpMessagesSendingOperations class that provides methods for sending
     * message to users
     */
    @Autowired
    private SimpMessagingTemplate template;

    /**
     * This method uses the SimpleMessagingTemplate instance to call the
     * convertAndSend method (a familiar method from other technologies). The
     * convertAndSend method requires a destination, in this case the topic
     * where the message will be sent, and the message itself. You may have
     * noticed that there is a / topic path before the topic’s name. This is the
     * way WebSockets will identify the topic name, by adding the /topic prefix.
     *
     * @param topic
     * @param message
     */
    public void sendMessageTo(String topic, String message) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(dateFormatter.format(new Date()));
        builder.append("] ");
        builder.append(message);
        this.template.convertAndSend("/topic/" + topic, builder.toString());
    }

}
